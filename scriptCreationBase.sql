CREATE TABLE Invite
(
	idInvite int AUTO_INCREMENT,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	email VARCHAR(70) NOT NULL,
	promotion varchar(4),
	entreprise varchar(30),
	typeInvite VARCHAR(20),
	nbAccompagnant int,
	PRIMARY KEY(idInvite)
);

CREATE TABLE Accompagnant
(
	idAccompagnant int AUTO_INCREMENT,
	idInvite int,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	PRIMARY KEY (idAccompagnant),
	FOREIGN KEY (idInvite) REFERENCES Invite(idInvite)
);

CREATE TABLE Message
(
	idMessage int AUTO_INCREMENT,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	email VARCHAR(70) NOT NULL,
	message text NOT NULL,
	PRIMARY KEY(idMessage)
);
