var myApp = angular.module('app', ['ui.router','ngMessages']).controller('crdCtrl', function ($scope, $http) {
	$scope.nbAccompagnant=['0','1', '2', '3', '4'];
	$scope.showNbAccomp=false;
	$scope.showNomAccomp=false;
	$scope.showAnneeDiplome=false;
	$scope.showNomEntreprise=false;
	var fields = [];
	//va contenir toutes les valeurs de notre formulaire
	$scope.formData = {};
	$scope.formData.dynamicFields=fields;
	$scope.SentValues;
	$scope.nbAccomp = 0;

	$scope.addNewInvite = function (formValues) {
		$scope.SentValues=formValues.dynamicFields;
		$http.post("/addInvite", JSON.stringify({prenom: $scope.prenom, nom: $scope.nom, mail: $scope.mail,  nbAccompagnant: $scope.nbAccomp, type: $scope.type, promotion:$scope.promotion, entreprise:$scope.entreprise, accompagnant : $scope.SentValues})).then(function (response) {
			console.log(response.data);
			if(response.data == "Ok"){
				alert("Votre inscription a été prise en compte. Vous allez recevoir un e-mail.");
				document.getElementById('nom').value='';
				document.getElementById('prenom').value='';
				document.getElementById('mail').value='';
				window.location.reload();
			}
			else {
				alert("Une erreur est survenue, merci de nous contacter par mail : ceremonie-diplome@toulouse.miage.fr")
			}

		});
		console.log($scope.SentValues);


	};

	$scope.modifType = function(){
		if($scope.type=="Etudiant"){
			$scope.showAnneeDiplome=true;
			$scope.showNomEntreprise=false;
			document.getElementById('entreprise').value='';
		}
		else if($scope.type=="Diplomes"){
			$scope.showAnneeDiplome=true;
			$scope.showNomEntreprise=true;
		}
		else if($scope.type=="Entreprise"){
			$scope.showAnneeDiplome=false;
			$scope.showNomEntreprise=true;
			$scope.showNomAccomp=false;
			document.getElementById('promotion').value='';

		}
		else{
			$scope.showAnneeDiplome=false;
			$scope.showNomEntreprise=false;
			$scope.showNomAccomp=false;
			document.getElementById('promotion').value='';
			document.getElementById('entreprise').value='';
		}
	};

	$scope.modifAnneeDiplome = function(){
		if($scope.promotion=="2019"){
			$scope.showNomAccomp=true;
		}
		else{
			$scope.showNomAccomp=false;
			$scope.formData.dynamicFields.length=0;
			nbAccomp=0;
		}
	};

	$scope.addAccompagnant=function(){
		$scope.nbAccomp++;
		var newItemNum = $scope.formData.dynamicFields.length+1;
		$scope.formData.dynamicFields.push({name: 'Accompagnant '+newItemNum, nom: '', prenom :''});
	}

	$scope.addNewMessage = function() {
		$http.post("/addMessage", JSON.stringify({prenom: $scope.prenomCont, nom: $scope.nomCont, mail: $scope.mailCont,  message : $scope.message})).then(function (response) {
			if(response.data == "Ok"){
				alert("Votre message a été envoyé!");
				window.location.reload();
			}

		});

	};

});
