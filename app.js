var express = require('express'),
        app = express(),
        port = parseInt(process.env.PORT, 10) || 80;
var mysql = require('mysql');
var bodyParser = require('body-parser');
var nodeMailer = require('nodemailer');
var handlebars = require('handlebars');
var fs = require('fs');

app.listen(port);
app.use(express.static((__dirname + '/public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Connexion à MySQL
var mySqlClient = mysql.createConnection({
  host: "miage-toulouse-remise-diplome-2019-mysqldbserver.mysql.database.azure.com",
  user: "admCRDBD19@miage-toulouse-remise-diplome-2019-mysqldbserver",
  password: "ZuperAdmDB#1",
  database: "miagecrd",
  port: 3306
});

//Connexion à MySQL - LOCAL
/*
var mySqlClient = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "SiteWebCRD",
  port: 3306
});
*/

setInterval(function(){
  mySqlClient.query('SELECT 1', function(err, result, fields){
    	if(err) throw err;
  });
  console.log("Ok")
},10000);

//Lecture d'un fichier
var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

var content = fs.readFileSync(__dirname + '/ceremonieDiplome.ics', 'utf8');

//Ajout d'un invité
app.post('/addInvite', function(req, res){

  //mySqlClient.connect();
  //Ajout en base
  var ite;
  var ok=1;
	mySqlClient.query('INSERT INTO Invite (nom, prenom, email, promotion, entreprise,typeInvite, nbAccompagnant) VALUES (?,?,?,?,?,?,?)', [req.body.nom,req.body.prenom,req.body.mail, req.body.promotion, req.body.entreprise,  req.body.type, req.body.nbAccompagnant], function(err, result, fields){
		if(err) {
      throw err;
      ok=0;
    }
    else {
      if(req.body.type=="Diplomes")
      {
        for(ite=0;ite<req.body.accompagnant.length;ite++){
          if(req.body.accompagnant[ite].nom!= "" && req.body.accompagnant[ite].prenom!=""){
            var accomp=[req.body.accompagnant[ite].nom, req.body.accompagnant[ite].prenom, result.insertId]
            mySqlClient.query("INSERT INTO Accompagnant (nom, prenom, idInvite) VALUES (?,?,?)", [req.body.accompagnant[ite].nom, req.body.accompagnant[ite].prenom, result.insertId], function(err, result, fields){
              if(err) {
                ok=0;
                throw err;
              }
              else {
                  ok=1;
              }
            });
          }
        }
      }
    }
    if(ok==0){
      res.send("KO");
    }
    else {
      res.send("Ok");
      var transporter = nodeMailer.createTransport(
      {
        service: "Outlook365",
        auth:{
          user:'ceremonie-diplome@toulouse.miage.fr',
          pass:'JWsxw783'
        },
        tls: {rejectUnauthorized: false},
        debug:true
      }
    );
    //Envoi de l'email de confirmation
    readHTMLFile(__dirname + '/public/email.html', function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
            name: req.body.nom,
            surname: req.body.prenom
        };
        var htmlToSend = template(replacements);
        var mailOptions = {
            from: 'ceremonie-diplome@toulouse.miage.fr',
            to: req.body.mail,
            subject: 'Confirmation inscription CRD 2019',
            html: htmlToSend
        };
        transporter.sendMail(mailOptions, function (error, response) {
            if (error) {
                console.log(error);
            }
        });
    });

    transporter.close();
    }
	});
});




app.post('/addMessage', function(req, res){
  console.log(req.body  );
  var transporter=nodeMailer.createTransport(
    {
      service: "Outlook365",
      auth:{
        user:'ceremonie-diplome@toulouse.miage.fr',
        pass:'JWsxw783'
      },
      tls: {rejectUnauthorized: false},
      debug:true
    }
  );
  var mailOptions = {
    from: req.body.mail,
    to: 'ceremonie-diplome@toulouse.miage.fr',
    subject: '[CDR] Message de ' + req.body.nom + ' ' + req.body.prenom,
    text: 'Mail : ' + req.body.mail + '<br/>' +req.body.message,
    html:'<b>' + 'Mail : ' + req.body.mail + '<br/> <br/>' + req.body.message +'</b>'
  };

  transporter.sendMail(mailOptions, function(error, info){
    if(error){
      return console.log(error);
    }
    console.log('Message sent ' + info.response);
  });
  transporter.close();

  mySqlClient.query("INSERT INTO Message (nom, prenom, email, message) VALUES(?,?,?,?)", [req.body.nom, req.body.prenom, req.body.mail, req.body.message], function(err, result){
    if(err) {
      throw err;
    }
    else {
      res.send("Ok");
    }
  });
});

module.exports=app;
